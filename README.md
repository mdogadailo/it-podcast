# IT Podcasts #

### Active
| Name                   | Link                                                                                            | Topics                |
|:-----------------------|:------------------------------------------------------------------------------------------------|:----------------------|
| DevZen                 | [iTunes](https://itunes.apple.com/ru/podcast/devzen-podcast/id903660317)                        |                       |
| GolangShow             | [iTunes](https://itunes.apple.com/ru/podcast/podkast-golangshow/id1031101295)                   | Go                    |
| Бананы и линзы         | [iTunes](https://itunes.apple.com/ru/podcast/banany-i-linzy/id1037879859)                       | Haskell               |
| Радио-Т                | [iTunes](https://itunes.apple.com/ru/podcast/radio-t/id256504435)                               |                       |
| SDCast                 | [iTunes](https://itunes.apple.com/ru/podcast/software-development-podcast/id890468606)          |                       |
| RWpod                  | [iTunes](https://itunes.apple.com/ru/podcast/rwpod-podkast-pro-mir-ruby/id597248066)            |                       |
| Разбор Полетов         | [iTunes](https://itunes.apple.com/ru/podcast/podkast-razbor-poletov/id594292319)                | Ruby                  |
| Как делают игры        | [iTunes](https://itunes.apple.com/ru/podcast/kak-delaut-igry-podkast-galyonkin.com/id560881209) |                       |
| Радиома                | [iTunes](https://itunes.apple.com/ru/podcast/razvlekatel-nyj-it-podkast/id526797445)            |                       |
| Суровый веб            | [iTunes](https://itunes.apple.com/ru/podcast/uwebdesign-mysli-ob-it-i-web/id923355344)          |                       |
| Пятиминутка React      | [iTunes](https://itunes.apple.com/ru/podcast/patiminutka-react/id1178897992)                    |                       |
| linkmeup               | [iTunes](https://itunes.apple.com/ru/podcast/linkmeup.-pervyj-podkast-dla/id1065445951)         |                       |
| The Art Of Programming | [iTunes](https://itunes.apple.com/ru/podcast/the-art-of-programming/id1046278525)               |                       |
| Веб-стандарты          | [iTunes](https://itunes.apple.com/ru/podcast/veb-standarty/id1080500016)                        | HTML, CSS, JavaScript |
| Android Dev            | [iTunes](https://itunes.apple.com/ru/podcast/android-dev-podkast/id1076856310)                  |  Android              |
| Scalalaz               | [iTunes](https://itunes.apple.com/ru/podcast/scalalaz-podcast/id1156356598)                     | Scala                 |
| Appleinsider           | [iTunes](https://itunes.apple.com/ru/podcast/id427483050)                                       |                       |
| Frontend Weekend       | [iTunes](https://itunes.apple.com/ru/podcast/frontend-weekend/id1233996390)                     | HTML, CSS, JavaScript |
| ДевШахта               | [iTunes](https://itunes.apple.com/de/podcast/девшахта/id1226773343)                             | HTML, CSS, JavaScript |
| Podlodka               | [iTunes](https://itunes.apple.com/us/podcast/podlodka-podcast/id1209828744)                     |                       |
| RawMind                | [iTunes](https://itunes.apple.com/ru/podcast/rawmind/id979046733)                               |                       |
| Фронтенд Юность        | [iTunes](https://itunes.apple.com/ru/podcast/фронтенд-юность-18/id1247192730)                   |                       |
| Sebrant chatting       | [iTunes](https://itunes.apple.com/ru/podcast/sebrant-chatting/id1320623324)                     |                       |
| QA Guild               | [iTunes](https://itunes.apple.com/ua/podcast/qaguild/id1350668092)                              |                       |

### Dead
| Name                   | Link                                                                                            | Topics                |
|------------------------|-------------------------------------------------------------------------------------------------|-----------------------|
| EaxCast                | [iTunes](https://itunes.apple.com/ru/podcast/eaxcast/id808411085)                               |                       |
| Frontflip              | [iTunes](https://itunes.apple.com/ru/podcast/frontflip/id884716456)                             | JavaScript, HTML, CSS |
| RadioJS                | [iTunes](https://itunes.apple.com/ru/podcast/radio-js/id904938655)                              | JavaScript            |
| RadioFlazm             | [iTunes](https://itunes.apple.com/ru/podcast/radioflazm/id709222897)                            |                       |
| Radio QA               | [iTunes](https://itunes.apple.com/podcast/radio-qa/id1021236121)                                |                       |
| Solo on .NET           | [iTunes](https://itunes.apple.com/ru/podcast/solo-on-.net/id730081104)                          | .NET                  |
| CTOcast                | [iTunes](https://itunes.apple.com/ru/podcast/ctocast/id945496997)                               |                       |
| RubyNoName             | [iTunes](https://itunes.apple.com/ru/podcast/ruby-noname-podcast/id581390515)                   | Ruby                  |
| DevOps Дефлопе         | [iTunes](https://itunes.apple.com/ru/podcast/devops-deflope-podkast/id670175970)                |  DevOps               |
| Пятиминутка PHP        | [iTunes](https://itunes.apple.com/ru/podcast/patiminutka-php/id996423650)                       | PHP                   |
| Диалоги #поИБэ         | [iTunes](https://itunes.apple.com/ru/podcast/dialogi-poibe/id869831341)                         |                       |
| jff name               | [iTunes](https://itunes.apple.com/ru/podcast/podkast-pro-frilans-jff.name/id1015952378)         |                       |
| Хекслет                | [iTunes](https://itunes.apple.com/ru/podcast/hekslet/id1162673070)                              |                       |
| Происхождение видов    | [iTunes](https://itunes.apple.com/ru/podcast/происхождение-видов/id1282666034)                  |                       |
| SPB Frontend Drinkcast | [iTunes](https://itunes.apple.com/ru/podcast/spb-frontend-drinkcast/id1269128794)               |                       |